require 'discordrb'

$bot = Discordrb::Bot.new token: ENV['TOKEN']

arr = ["woof woof!", "woof", "WOOF!", "wo0f", "meow", "wo0f", "woof woof woof", "wooff"]
$bot.message() do |event|
  event.respond arr.shuffle.first
end

$bot.run()